function varargout = BCM_Control(varargin)
% BCM_CONTROL MATLAB code for BCM_Control.fig
%      BCM_CONTROL, by itself, creates a new BCM_CONTROL or raises the existing
%      singleton*.
%
%      H = BCM_CONTROL returns the handle to a new BCM_CONTROL or the handle to
%      the existing singleton*.
%
%      BCM_CONTROL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BCM_CONTROL.M with the given input arguments.
%
%      BCM_CONTROL('Property','Value',...) creates a new BCM_CONTROL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BCM_Control_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BCM_Control_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BCM_Control

% Last Modified by GUIDE v2.5 25-Jan-2018 14:39:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BCM_Control_OpeningFcn, ...
                   'gui_OutputFcn',  @BCM_Control_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before BCM_Control is made visible.
function BCM_Control_OpeningFcn(hObject, eventdata, handles, varargin)

global gain pulse Invert_Output_flag Invert_Pulse_flag Calibration_flag
global histo_charge_Gun   histo_charge_Vesper
global monitorBCM
global axes1 useLog

axes1 = handles.axes1 ;
useLog = false;

% initial settings
gain = '6dB';
pulse = '1nC';
Invert_Output_flag   = false ;
Invert_Pulse_flag    = false ;
Calibration_flag     = false ;


histo_charge_Gun    = zeros(1, 30) ;
histo_charge_Vesper = zeros(1, 30) ;

% Choose default command line output for BCM_Control
handles.output = hObject;

set(handles.Gain_Selection,    'SelectionChangeFcn',   @Gain_Selection_SelectionChangeFcn) ;         %%%% TRES IMPORTANT %%%%

set(handles.Pulse_Selection,    'SelectionChangeFcn',  @Pulse_Selection_SelectionChangeFcn) ;         %%%% TRES IMPORTANT %%%%


Channels_names = {...
    'CA.SABCM01/Samples#samples',...            % mesure from Gun BCM 
    'CA.SABPMCAL-SIS5-2/Samples#samples',...    % mesure from VESPER BCM 
    'CA.BCM01GAIN/Setting#enumValue',...        % Current BCM setting
     } ;

monitorBCM = matlabJapcMonitor('SCT.USER.SETUP',Channels_names, @(dati,monitorObject)Read_BCM(dati));
monitorBCM.useFastStrategy(1);
monitorBCM.setFastStrategyTimeOut(180);
monitorBCM.useRawDataStruct(1); % simplified structure with data
monitorBCM.start();


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes BCM_Control wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = BCM_Control_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

SetBCMconfig()


function Gain_Selection_SelectionChangeFcn(hObject, eventdata)
global gain

handles = guidata(hObject) ;

switch get(eventdata.NewValue,'Tag')
    case 'Gain_6dB'
        gain  = '6dB' ;
    case 'Gain_12dB'
        gain  = '12dB' ;
    case 'Gain_18dB'
        gain  = '18dB' ;
    case 'Gain_20dB'
        gain  = '20dB' ;
    case 'Gain_26dB_1'
        gain  = '26dB1' ;
    case 'Gain_26dB_2'
        gain  = '26dB2' ;
    case 'Gain_32dB'
        gain  = '32dB' ;
    case 'Gain_40dB'
        gain  = '40dB' ;
end
SetBCMconfig


function Pulse_Selection_SelectionChangeFcn(hObject, eventdata)
global pulse

handles = guidata(hObject) ;

switch get(eventdata.NewValue,'Tag')
    case 'Pulse_1nC'
        pulse  = '1nC' ;
    case 'Pulse_100pC'
        pulse  = '100pC' ;
    case 'Pulse_10pC'
        pulse  = '10pC' ;
    case 'Pulse_1pC'
        pulse  = '1pC' ;
end
SetBCMconfig

% --- Executes on button press in Gain_6dB.
function Gain_6dB_Callback(hObject, eventdata, handles)
% Hint: get(hObject,'Value') returns toggle state of Gain_6dB

% --- Executes on button press in Gain_12dB.
function Gain_12dB_Callback(hObject, eventdata, handles)

% --- Executes on button press in Gain_18dB.
function Gain_18dB_Callback(hObject, eventdata, handles)

% --- Executes on button press in Gain_20dB.
function Gain_20dB_Callback(hObject, eventdata, handles)

% --- Executes on button press in Gain_26dB_1.
function Gain_26dB_1_Callback(hObject, eventdata, handles)

% --- Executes on button press in Gain_26dB_2.
function Gain_26dB_2_Callback(hObject, eventdata, handles)

% --- Executes on button press in Gain_32dB.
function Gain_32dB_Callback(hObject, eventdata, handles)

% --- Executes on button press in Gain_40dB.
function Gain_40dB_Callback(hObject, eventdata, handles)



% --- Executes on button press in Invert_Output.
function Invert_Output_Callback(hObject, eventdata, handles)

global  Invert_Output_flag
Invert_Output_flag = get(hObject,'Value') ;
SetBCMconfig

% --- Executes on button press in Calibration_On.
function Calibration_On_Callback(hObject, eventdata, handles)

global  Calibration_flag
Calibration_flag = get(hObject,'Value') ;
SetBCMconfig

% --- Executes on button press in Invert_Pulse.
function Invert_Pulse_Callback(hObject, eventdata, handles)

global  Invert_Pulse_flag
Invert_Pulse_flag = get(hObject,'Value') ;
SetBCMconfig


function SetBCMconfig

global gain pulse Invert_Output_flag Invert_Pulse_flag Calibration_flag

matlabJapc.staticSet('SCT.USER.SETUP', 'CA.BCM01GAIN', 'Setting', 'enumValue', gain)

if Invert_Output_flag
    matlabJapc.staticSet('SCT.USER.SETUP', 'CA.BCM01OUTPOL', 'Setting', 'enumValue', 'INV')
else
    matlabJapc.staticSet('SCT.USER.SETUP', 'CA.BCM01OUTPOL', 'Setting', 'enumValue', 'NOINV')
end

if Invert_Pulse_flag
    matlabJapc.staticSet('SCT.USER.SETUP', 'CA.BCM01CALPOL', 'Setting', 'enumValue', 'NEG')
else
    matlabJapc.staticSet('SCT.USER.SETUP', 'CA.BCM01CALPOL', 'Setting', 'enumValue', 'POS')
end

matlabJapc.staticSet('SCT.USER.SETUP', 'CA.BCM01CALCHARGE', 'Setting', 'enumValue', pulse)

if Calibration_flag
    matlabJapc.staticSet('SCT.USER.SETUP', 'CA.BCM01CALENABLE', 'Setting', 'enumValue', 'ON')
else
    matlabJapc.staticSet('SCT.USER.SETUP', 'CA.BCM01CALENABLE', 'Setting', 'enumValue', 'OFF')
end



% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Shutdown the listner
global monitorBCM;
monitorBCM.stop();
disp('Stopping and deleting JAPC monitor "global monitorBCM"');
% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2
global useLog
useLog=get(hObject,'Value');
