function [newptcharge_Gun] = read_single_bcm( BCM_name )
    BCM_sensitivity = [2.085  4.18  8.35  10.42  20.97 20.95 41.9  105.0] ; % in [V/nC] from self calibration pulse
    
    if strcmp(BCM_name,'vesper')
        BCM_offset = [0.033  0.014 0.01  0.000 0.005 0.005 0.002 0.003] ; % in [nC] from no laser beam September 06, 2016
        signal = matlabJapc.staticGetSignal('SCT.USER.SETUP','CA.SABPMCAL-SIS5-2/Samples#samples');
    elseif strcmp(BCM_name,'gun')
        BCM_offset    = [-0.004  -0.005 -0.003  -0.004  -0.004 -0.005 -0.005 0.002] ; % in [V] from scope high impedance
        signal = matlabJapc.staticGetSignal('SCT.USER.SETUP','CA.SABCM01/Samples#samples');
    else
        disp(['bad BCM_name "',BCM_name,'"']);
        return
    end
    
    %matlabJapcMonitor('SCT.USER.SETUP',Channels_names, @(dati,monitorObject)Read_BCM(dati))
    BCM_value =  signal / 1000 ; % [V] 
    
    plot(signal)
    
    BCM_gain = matlabJapc.staticGetSignal('SCT.USER.SETUP','CA.BCM01GAIN/Setting#enumValue');
    
    BCM_value = mean(BCM_value(1, 4000:8000)) * 2.13;  % factor 2.13 between ADC (50 ohms) and oscilloscope (high impedance)
    
    switch BCM_gain
        case '6dB'
            indx = 1 ; 
        case '12dB'
            indx = 2 ;        
        case '18dB'
            indx = 3 ;  
        case '20dB'
            indx = 4 ;           
        case '26dB1'
            indx = 5 ; 
        case '26dB2'
            indx = 6 ; 
        case '32dB'
            indx = 7 ;    
        case '40dB'
            indx = 8 ;
    end
    
    newptcharge_Gun = (BCM_value / BCM_sensitivity(indx)) ;  % - BCM_Gun_offset(indx) )  ;   % in [nC]
    

end