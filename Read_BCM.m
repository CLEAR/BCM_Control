function Read_BCM( dati )

global  histo_charge_Gun   histo_charge_Vesper
global 	axes1 useLog
% global  newptchargeBCM_Gun  newptchargeBCM_Vesper
% global  BCM_gain


%BCM_sensitivity = [0.200  0.400  0.800  1.000  2.005 2.005 4.009  10.037] ; % in [V/nC] from Bergoz Certificate of Calibration March 11, 2016
%BCM_Vesper_offset = [0.033  0.014 0.01  0.000    0.005 0.005 0.002 -0.003] ; % in [nC] from no laser beam September 06, 2016

BCM_sensitivity = [2.085  4.18  8.35  10.42  20.97 20.95 41.9  105.0] ; % in [V/nC] from self calibration pulse

BCM_Vesper_offset = [0.033  0.014 0.01  0.000    0.005 0.005 0.002 0.003] ; % in [nC] from no laser beam September 06, 2016
BCM_Gun_offset    = [-0.004  -0.005 -0.003  -0.004  -0.004 -0.005 -0.005 0.002] ; % in [V] from scope high impedance

% 6  12  18  20  26a  26b 32 40 dB


BCM1_value =  dati.values{1} / 1000 ; % [V] 
BCM2_value =  dati.values{2} / 1000 ; % [V], negative sign since BCM installed in reverse position

BCM_gain = dati.values{3} ;


BCM1_value = mean(BCM1_value(1, 4000:8000)) * 2.13 ; % factor 2.13 between ADC (50 ohms) and oscilloscope (high impedance)
BCM2_value = mean(BCM2_value(1, 4000:8000)) * 2.13 ;


switch BCM_gain
    case '6dB'
        indx = 1 ; 
    case '12dB'
        indx = 2 ;        
    case '18dB'
        indx = 3 ;  
    case '20dB'
        indx = 4 ;           
    case '26dB1'
        indx = 5 ; 
    case '26dB2'
        indx = 6 ; 
    case '32dB'
        indx = 7 ;    
    case '40dB'
        indx = 8 ;
end


newptchargeBCM_Gun = (BCM1_value / BCM_sensitivity(indx)) ;  % - BCM_Gun_offset(indx) )  ;   % in [nC]
newptchargeBCM_Vesper = (BCM2_value / BCM_sensitivity(indx)) ;  % - BCM_Vesper_offset(indx) )   


histo_charge_Gun = [histo_charge_Gun(2:end), newptchargeBCM_Gun] ;
histo_charge_Vesper = [histo_charge_Vesper(2:end), newptchargeBCM_Vesper] ; 

moy_Gun     = mean(histo_charge_Gun(end-5:end)) ;
moy_Vesper  = mean(histo_charge_Vesper(end-5:end)) ;


if useLog
    semilogy(axes1, abs(histo_charge_Gun), '.r-')
    hold(axes1, 'on')
    semilogy(axes1, abs(histo_charge_Vesper), '.m-')
    hold(axes1, 'off')
    text(length(histo_charge_Vesper)-3, abs(histo_charge_Vesper(end))*1.5, ...
         [num2str(0.1 * round(1e4 * histo_charge_Vesper(end))), ' pC'], ...
        'Color', 'm', 'Parent', axes1)
    text(length(histo_charge_Gun)-3, abs(histo_charge_Gun(end))*0.5, ...
        [num2str(0.1 * round(1e4 * histo_charge_Gun(end))), ' pC'], ...
        'Color', 'r', 'Parent', axes1)
    ylabel(axes1, 'abs(Charge) [nC]')
else
    plot(axes1, histo_charge_Gun, '.r-')
    hold(axes1, 'on')
    plot(axes1, histo_charge_Vesper, '.m-')
    hold(axes1, 'off')
    text(length(histo_charge_Vesper)-3, histo_charge_Vesper(end)*1.5, ...
        [num2str(0.1 * round(1e4 * histo_charge_Vesper(end))), ' pC'], ...
        'Color', 'm', 'Parent', axes1)
    text(length(histo_charge_Gun)-3, histo_charge_Gun(end)*0.5, ...
        [num2str(0.1 * round(1e4 * histo_charge_Gun(end))), ' pC'], ...
        'Color', 'r', 'Parent', axes1)
    ylabel(axes1, 'Charge [nC]')

end
grid(axes1)
legend(axes1, 'Gun', 'Vesper', 'Location', 'NorthWest')


end

